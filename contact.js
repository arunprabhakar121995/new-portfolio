const passwordinput = document.getElementById("password")
const minlength = document.getElementById("minlength")
const maxlength = document.getElementById("maxlength")
const charactercheck = document.getElementById("charactercheck")
passwordinput.addEventListener("keyup", verifypassword)

function verifypassword(){
    const password = passwordinput.value
    if(password.length <6){
        minlength.classList.remove("green")
        minlength.innerHTML="&#x2718;"
    }
    else{
        minlength.classList.add("green")
        minlength.innerHTML="&#x2714;"
    }

    //max 20 characters

    if(password.length > 20){
        maxlength.classList.remove("green")
        maxlength.innerHTML="&#x2718;"
    }
    else{
        maxlength.classList.add("green")
        maxlength.innerHTML="&#x2714;"
    }


    //characters check

    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()\-_=+{};:,<.>]).{8,}$/;
    
    if(regex.test(password)){

        charactercheck.classList.add("green")
        charactercheck.innerHTML="&#x2714;"
    }
    else{
        charactercheck.classList.remove("green")
        charactercheck.innerHTML="&#x2718;"
    }
}